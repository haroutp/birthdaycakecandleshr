﻿using System;

namespace BirthdayCakeCandles
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(birthdayCakeCandles(new int[]{44, 53, 31, 27, 77, 60, 66, 77, 26, 36}));

        }

        static int birthdayCakeCandles(int[] ar) {
            int max = GetMax(ar);
            int count = 0;

            for(int i = 0; i < ar.Length; i++){
                if(ar[i] == max){
                    count++;
                }
            }

            return count;
        }

        static int GetMax(int[] ar){
            int max = ar[0];
            for(int i = 0; i < ar.Length; i++){
                if(max < ar[i]){
                    max = ar[i];
                }
            }

            return max;
        }
    }
}
